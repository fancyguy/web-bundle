<?php
/**
 * This file is part of FancyGuy Web Bundle.
 *
 * Copyright (c) 2015 Steve Buzonas <steve@fancyguy.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FancyGuy\Bundle\WebBundle;

use FancyGuy\Bundle\DistributionBundle\HttpKernel\Bundle;
use FancyGuy\Bundle\DistributionBundle\HttpKernel\BundleCollection;

class FancyGuyWebBundle extends Bundle
{
    public function getDependencies()
    {
        return new BundleCollection(
            new \FancyGuy\Bundle\DistributionBundle\FancyGuyDistributionBundle(),
            new \Symfony\Bundle\TwigBundle\TwigBundle(),
            new \Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle()
        );
    }
}
